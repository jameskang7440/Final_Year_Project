# COMP3931 Final Year Project - Elder studios: Be a developer

## General Description

- The content of the project is to construct making a source code by the flowchart for the procedural programming beginners.

##### Client(User)

- A user is able to create the actual programming language source code by the user-created flow chart.

##### Server

- Server gets the information of the flowchart from the client side and translates into the pseudo-code. After that, it generates source code of the client-selected programming language.
- Server shows the result of the source code with the flowchart. Besides, server prevents the illegal way of generating the flowchart.

## Use this web server/application
- Install STS first
- Then download this project
- Import this project from STS
- Also, set the apache tomcat environment
- before run, go to server setting (double click 'Tomcat ..." in the server area), click modules and change path by '/'
- run by server and enjoy it.

## Contact
* Janghoon Kang [sc15j2k@leeds.ac.uk]
