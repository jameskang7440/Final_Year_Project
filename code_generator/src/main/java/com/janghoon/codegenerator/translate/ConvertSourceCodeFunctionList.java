package com.janghoon.codegenerator.translate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class ConvertSourceCodeFunctionList {
	
	// this method decides that the string is consist of the number only
	public boolean isStringNumber(String s) {
		try {
			Double.parseDouble(s);
			return true;
		}catch(Exception e) {
			return false;
		}
	}
	
	// combine the method part and main part in JAVA
	public String combineJavaSourceCode(String sourceCode, HashMap<String,String> variables) {
		String methodPart = sourceCode.split("Start")[0];
		String mainPart = "Start\n"+sourceCode.split("Start")[1];
		String fixedMethodPart = "";
		String fixedMainPart = "";
		String[] methodPartLines = methodPart.split("\\n");
		String[] mainPartLines = mainPart.split("\\n");
		// method part
		for(String line : methodPartLines) {
			if(line.contains("Func")) {
				int parameter_start_index = line.indexOf("(");
				String parameter = line.substring(parameter_start_index).replace("{", "");
				String modifiedParameter = "";
				if(parameter.contains(",")) {
					String[] parameterFragments = parameter.replace("(", "").replace(")", "").split(",");
					modifiedParameter = "(";
					for(String item : parameterFragments) {
						String parameter_type = variables.get(item);
						modifiedParameter+=parameter_type+" "+item+",";
					}
					modifiedParameter+=")";
					modifiedParameter = modifiedParameter.replace(",)", ")");
				}else {
					if(variables.containsKey(parameter.replace(" (", "").replace(")", ""))) {
						String parameter_type = variables.get(parameter.replace(" (", "").replace(")", ""));
						modifiedParameter = "("+parameter_type+" "+parameter.replace(" (", "").replace(")", "")+")";
					}else {
						modifiedParameter = parameter;
					}
				}
				fixedMethodPart +="\t"+line.substring(0,parameter_start_index).replace("Func", "")+modifiedParameter+"{\n";
			}else {
				fixedMethodPart+="\t"+line+"\n";
			}
				
		}
		// main part
		for(String line : mainPartLines) {
			if(line.contains("Start")) {
				fixedMainPart+="\tpublic static void main(String[] args){\n\t\t";
			}else if(line.contains("End")) {
				fixedMainPart+="\n\t}";
			}else {
				fixedMainPart+=line+"\n\t\t";
			}
		}
		return "public class Result {\n"+fixedMethodPart+"\n"+fixedMainPart+"\n}";
	}
	
	// convert the print method and array-related methods into source code and combine the method part and main part in C
	public String combineCsourceCode(String sourceCode, HashMap<String,String> arrayType, HashMap<String,Integer> arraySize, HashMap<String,String> variables, HashMap<String,String> methodList) {
		String methodPart = sourceCode.split("Start")[0];
		String mainPart = "Start\n"+sourceCode.split("Start")[1];
		ArrayList<String> globalVariables = new ArrayList<>();
		String globalVarPart = "";
		String fixedMethodPart = "";
		String fixedMainPart = "";
		String[] methodPartLines = methodPart.split("\\n");
		String[] mainPartLines = mainPart.split("\\n");
		// method part
		for(String line : methodPartLines) {
			if(line.contains("Func")) {
				int parameter_start_index = line.indexOf("(");
				String parameter = line.substring(parameter_start_index).replace("{", "");
				String modifiedParameter = "";
				if(parameter.contains(",")) {
					String[] parameterFragments = parameter.replace("(", "").replace(")", "").split(",");
					modifiedParameter = "(";
					for(String item : parameterFragments) {
						String parameter_type = variables.get(item);
						modifiedParameter+=parameter_type+" "+item+",";
					}
					modifiedParameter+=")";
					modifiedParameter = modifiedParameter.replace(",)", ")");
				}else {
					if(variables.containsKey(parameter.replace(" (", "").replace(")", ""))) {
						String parameter_type = variables.get(parameter.replace(" (", "").replace(")", ""));
						modifiedParameter = "("+parameter_type+" "+parameter.replace(" (", "").replace(")", "")+")";
					}else {
						modifiedParameter = parameter;
					}
				}
				fixedMethodPart += line.substring(0,parameter_start_index).replace("Func", "")+modifiedParameter+"{\n";
			}
			else if(line.contains("printf")) {
				int tab_index = line.indexOf("p");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index);
				int leftBracket = codeLine.indexOf("(");
				int rightBracket = codeLine.indexOf(")");
				String content = codeLine.substring(leftBracket+1,rightBracket);
				if(variables.containsKey(content)) {
					if(variables.get(content).contains("int")) {
						fixedMethodPart+=tab+"printf(\"%d\\n\","+content+");\n";
					}else if(variables.get(content).contains("double") || variables.get(content).contains("float")) {
						fixedMethodPart+=tab+"printf(\"%f\\n\","+content+");\n";
					}else if(variables.get(content).contains("char")) {
						fixedMethodPart+=tab+"printf(\"%c\\n\","+content+");\n";
					}
				}else if(arrayType.containsKey(content.replace("[", "").replace("]", ""))) {
					if(content.contains("[") && content.contains("]")) {
						String arrayName = content.replace("[", "").replace("]", "");
						if(arrayType.get(arrayName).contains("int")) {
							fixedMethodPart+=tab+"printf(\"%d\\n\","+content+");\n";
						}else if(arrayType.get(arrayName).contains("double") || arrayType.get(arrayName).contains("float")) {
							fixedMethodPart+=tab+"printf(\"%f\\n\","+content+");\n";
						}else if(arrayType.get(arrayName).contains("char")) {
							fixedMethodPart+=tab+"printf(\"%c\\n\","+content+");\n";
						}
					}else {
						if(arrayType.get(content).contains("int")) {
							globalVariables.add("int len = 0;");
							fixedMethodPart+=tab+"len = sizeof("+content+")/sizeof("+content+"[0]);\n";
							fixedMethodPart+=tab+"for(array_location=0;array_location<len;array_location++){\n";
							fixedMethodPart+=tab+"\tprintf(\"%d\\n\","+content+"[array_location]);\n";
							fixedMethodPart+=tab+"}\n";
						}else if(arrayType.get(content).contains("double") || arrayType.get(content).contains("float")) {
							globalVariables.add("int len = 0;");
							fixedMethodPart+=tab+"len = sizeof("+content+")/sizeof("+content+"[0]);\n";
							fixedMethodPart+=tab+"for(array_location=0;array_location<len;array_location++){\n";
							fixedMethodPart+=tab+"\tprintf(\"%f\\n\","+content+"[array_location]);\n";
							fixedMethodPart+=tab+"}\n";	
						}else if(arrayType.get(content).contains("char")) {
							fixedMethodPart+=tab+"printf(\"%s\\n\","+content+");\n";
						}
					}
				}else if(methodList.containsKey(content.replace("(", ""))){
					String returnType = methodList.get(content.replace("(", "")).toLowerCase().split(";")[0];
					String parameter = methodList.get(content.replace("(", "")).toLowerCase().split(";")[1];
					switch(returnType) {
						case "int":
							fixedMainPart+=tab+"\tprintf(\"%d\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "double":
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "float":
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "char":
							fixedMainPart+=tab+"\tprintf(\"%c\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						default:
							break;
					}
				}else {
					if(isStringNumber(content)) {
						if(content.contains(".")) {
							fixedMethodPart+=tab+"printf(\"%f\\n\","+Double.parseDouble(content)+");\n";
						}else {
							fixedMethodPart+=tab+"printf(\"%d\\n\","+Integer.parseInt(content)+");\n";
						}
					}else {
						fixedMethodPart+=tab+"printf(\""+content+"\\n\");\n";
					}
				}
			}else if(line.contains("Array")) {
				globalVariables.add("int array_location = 0;");
				int tab_index = line.indexOf("A");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String type = fragments[0].replace("[", "").replace("]", "");
				String name = fragments[1].replace("[", "").replace("]", "");
				if(arraySize.get(name)==0) {
					fixedMethodPart+=tab+type+" "+name+"[10]={};\n";
				}else {
					fixedMethodPart+=tab+type+" "+name+"["+arraySize.get(name)+"]={};\n";					
				}
			}else if(line.contains("Append")) {
				int tab_index = line.indexOf("A");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String value = fragments[1].replace("[", "").replace("]", "");
				if(isStringNumber(value)) {
					fixedMethodPart+=tab+"for(array_location=0;array_location<"+arraySize.get(name)+";array_location++){\n";
					fixedMethodPart+=tab+"\tif("+name+"[array_location] == \'\\0\'){\n";
					fixedMethodPart+=tab+"\t\t"+name+"[array_location] = "+value+";\n";
					fixedMethodPart+=tab+"\t\tbreak;\n";
					fixedMethodPart+=tab+"\t}\n";
					fixedMethodPart+=tab+"}\n";
				}else {
					fixedMethodPart+=tab+"memmove("+name+"+strlen("+name+")+1,"+name+"+strlen("+name+"),1);\n";
					fixedMethodPart+=tab+name+"[strlen("+name+")]=\""+value+"\"\n";					
				}
			}else if(line.contains("Insert")) {
				int tab_index = line.indexOf("I");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String location = fragments[1].replace("[", "").replace("]", "");
				String value = fragments[2].replace("[", "").replace("]", "");
				if(isStringNumber(value)) {
					fixedMethodPart+=tab+"memmove("+name+"+"+location+"+1,"+name+"+"+location+",1*sizeof(int));\n";
					fixedMethodPart+=tab+name+"["+location+"]="+value+";\n";
				}else {
					fixedMethodPart+=tab+"memmove("+name+"+"+location+"+1,"+name+"+"+location+",strlen("+name+")-"+location+"+1);\n";
					fixedMethodPart+=tab+name+"["+location+"]=\""+value+"\";\n";
				}
			}else if(line.contains("Remove")) {
				int tab_index = line.indexOf("R");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String location = fragments[1].replace("[", "").replace("]", "");
				if(arrayType.get(name).contains("char")) {
					fixedMethodPart+=tab+"memmove("+name+"+"+location+","+name+"+"+location+"+1,strlen("+name+")-"+location+");\n";
				}else {
					globalVariables.add("int length;");
					fixedMethodPart+=tab+"length = sizeof("+name+")/sizeof("+name+"[0]);\n";
					fixedMethodPart+=tab+"memmove("+name+"+"+location+","+name+"+"+location+"+1,length-"+location+");\n";
				}
			}else {
				fixedMethodPart+=line+"\n";
			}
		}
		// main part
		for(String line : mainPartLines) {
			if(line.contains("printf")) {
				int tab_index = line.indexOf("p");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index);
				int leftBracket = codeLine.indexOf("(");
				int rightBracket = codeLine.indexOf(")");
				String content = codeLine.substring(leftBracket+1,rightBracket);
				if(variables.containsKey(content)) {
					if(variables.get(content).contains("int")) {
						fixedMainPart+=tab+"\tprintf(\"%d\\n\","+content+");\n";
					}else if(variables.get(content).contains("double") || variables.get(content).contains("float")) {
						fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content+");\n";
					}else if(variables.get(content).contains("char")) {
						fixedMainPart+=tab+"\tprintf(\"%c\\n\","+content+");\n";
					}
				}else if(arrayType.containsKey(content.replace("[", "").replace("]", ""))) {
					if(content.contains("[") && content.contains("]")) {
						String arrayName = content.replace("[", "").replace("]", "");
						if(arrayType.get(arrayName).contains("int")) {
							fixedMainPart+=tab+"\tprintf(\"%d\\n\","+content+");\n";
						}else if(arrayType.get(arrayName).contains("double") || arrayType.get(arrayName).contains("float")) {
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content+");\n";
						}else if(arrayType.get(arrayName).contains("char")) {
							fixedMainPart+=tab+"\tprintf(\"%c\\n\","+content+");\n";
						}
					}else {
						if(arrayType.get(content).contains("int")) {
							globalVariables.add("int len = 0;");
							fixedMainPart+=tab+"\tlen = sizeof("+content+")/sizeof("+content+"[0]);\n";
							fixedMainPart+=tab+"\tfor(array_location=0;array_location<len;array_location++){\n";
							fixedMainPart+=tab+"\t\tprintf(\"%d\\n\","+content+"[array_location]);\n";
							fixedMainPart+=tab+"\t}\n";
						}else if(arrayType.get(content).contains("double") || arrayType.get(content).contains("float")) {
							globalVariables.add("int len = 0;");
							fixedMainPart+=tab+"\tlen = sizeof("+content+")/sizeof("+content+"[0]);\n";
							fixedMainPart+=tab+"\tfor(array_location=0;array_location<len;array_location++){\n";
							fixedMainPart+=tab+"\t\tprintf(\"%f\\n\","+content+"[array_location]);\n";
							fixedMainPart+=tab+"\t}\n";
						}else if(arrayType.get(content).contains("char")) {
							fixedMainPart+=tab+"\tprintf(\"%s\\n\","+content+");\n";
						}
					}
				}else if(methodList.containsKey(content.replace("(", ""))){
					String returnType = methodList.get(content.replace("(", "")).toLowerCase().split(";")[0];
					String parameter = methodList.get(content.replace("(", "")).toLowerCase().split(";")[1];
					switch(returnType) {
						case "int":
							fixedMainPart+=tab+"\tprintf(\"%d\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "double":
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "float":
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						case "char":
							fixedMainPart+=tab+"\tprintf(\"%c\\n\","+content.replace("(", "")+parameter+");\n";
							break;
						default:
							break;
					}
				}else {
					if(isStringNumber(content)) {
						if(content.contains(".")) {
							fixedMainPart+=tab+"\tprintf(\"%f\\n\","+Double.parseDouble(content)+");\n";
						}else {
							fixedMainPart+=tab+"\tprintf(\"%d\\n\","+Integer.parseInt(content)+");\n";
						}
					}else {
						fixedMainPart+=tab+"\tprintf(\""+content+"\\n\");\n";
					}
				}
			}else if(line.contains("Array")) {
				globalVariables.add("int array_location = 0;");
				int tab_index = line.indexOf("A");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String type = fragments[0].replace("[", "").replace("]", "");
				String name = fragments[1].replace("[", "").replace("]", "");
				if(arraySize.get(name)==0) {
					fixedMainPart+=tab+"\t"+type+" "+name+"[10]={};\n";
				}else {
					fixedMainPart+=tab+"\t"+type+" "+name+"["+arraySize.get(name)+"]={};\n";					
				}
			}else if(line.contains("Append")) {
				int tab_index = line.indexOf("A");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String value = fragments[1].replace("[", "").replace("]", "");
				if(isStringNumber(value)) {
					fixedMainPart+=tab+"\tfor(array_location=0;array_location<"+arraySize.get(name)+";array_location++){\n";
					fixedMainPart+=tab+"\t\tif("+name+"[array_location] == \'\\0\'){\n";
					fixedMainPart+=tab+"\t\t\t"+name+"[array_location] = "+value+";\n";
					fixedMainPart+=tab+"\t\t\tbreak;\n";
					fixedMainPart+=tab+"\t\t}\n";
					fixedMainPart+=tab+"\t}\n";
				}else {
					fixedMainPart+=tab+"\tmemmove("+name+"+strlen("+name+")+1,"+name+"+strlen("+name+"),1);\n";
					fixedMainPart+=tab+"\t"+name+"[strlen("+name+")]=\'"+value+"\';\n";					
				}
			}else if(line.contains("Insert")) {
				int tab_index = line.indexOf("I");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String location = fragments[1].replace("[", "").replace("]", "");
				String value = fragments[2].replace("[", "").replace("]", "");
				if(isStringNumber(value)) {
					fixedMainPart+=tab+"\tmemmove("+name+"+"+location+"+1,"+name+"+"+location+",1*sizeof(int));\n";
					fixedMainPart+=tab+"\t"+name+"["+location+"]="+value+";\n";
				}else {
					fixedMainPart+=tab+"\tmemmove("+name+"+"+location+"+1,"+name+"+"+location+",strlen("+name+")-"+location+"+1);\n";
					fixedMainPart+=tab+"\t"+name+"["+location+"]=\'"+value+"\';\n";
				}
			}else if(line.contains("Remove")) {
				int tab_index = line.indexOf("R");
				String tab = line.substring(0,tab_index);
				String codeLine = line.substring(tab_index+7);
				String[] fragments = codeLine.split("\\s");
				String name = fragments[0].replace("[", "").replace("]", "");
				String location = fragments[1].replace("[", "").replace("]", "");
				if(arrayType.get(name).contains("char")) {
					fixedMainPart+=tab+"\tmemmove("+name+"+"+location+","+name+"+"+location+"+1,strlen("+name+")-"+location+");\n";
				}else {
					globalVariables.add("int length;");
					fixedMainPart+=tab+"\tlength = sizeof("+name+")/sizeof("+name+"[0]);\n";
					fixedMainPart+=tab+"\tmemmove("+name+"+"+location+","+name+"+"+location+"+1,length-"+location+");\n";
				}
			}else if(line.contains("Start")){
				fixedMainPart+="void main(int argc, char* argv[]){\n";
			}else if(line.contains("End")){
				fixedMainPart+="}\n";
			}else {
				fixedMainPart+="\t"+line+"\n";
			}
		}
		HashSet<String> removeDuplication = new HashSet<>(globalVariables);
		globalVariables = new ArrayList<>(removeDuplication);
		for(String variable : globalVariables) {
			globalVarPart+=variable+"\n";
		}
		return globalVarPart+"\n"+fixedMethodPart+"\n\n"+fixedMainPart;
	}
	
	// Python language conversion part
	public String python_language(String pseudocode) {
		String sourceCode = "";
		String[] pseudocodeLines = pseudocode.split("\\n");
		// Save the list of array, method and variable that uses in print part. Because, variables or arrays do not use ' " ' when it prints on the console.
		ArrayList<String> arrayLists = new ArrayList<>();
		ArrayList<String> variables = new ArrayList<>();
		HashMap<String, String> methods = new HashMap<>();
		// for each line in total code
		for(String line : pseudocodeLines) {
			String actualCode = "";
			if(line.contains("If")) {
				actualCode = line.replace("If", "if").replace("[", "").replace("]", "").replace("{", ":");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("While")) {
				actualCode = line.replace("While", "while").replace("[", "").replace("]", "").replace("{", ":");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("}else{")) {
				actualCode = line.replace("}", "").replace("{", ":");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("For")) {
				int tab_index = line.indexOf("F");
				String initialValue="";
				String specificValue = "";
				String stepValue = "";
				String[] items = line.split("]");
				for(String test : items) {
					if(test.contains("For")) {
						int initial_value_index = test.indexOf("[");
						initialValue = test.substring(initial_value_index+1);
					}else if(test.contains("to")) {
						int specific_value_index = test.indexOf("[");
						specificValue = test.substring(specific_value_index+1);
					}else if(test.contains("step")) {
						int step_value_index = test.indexOf("[");
						stepValue = test.substring(step_value_index+1);
					}
				}
				actualCode = line.substring(0,tab_index)+"for i in range("+initialValue+","+specificValue+","+stepValue+"):";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Print")) {
				int tabIndex = line.indexOf("P");
				String content = line.substring(tabIndex).replace("Print: ", "").replace("[","").replace("]", "");
				if(arrayLists.contains(content) || variables.contains(content)) {
					actualCode = line.replace("Print:", "print").replace("[","(").replace("]", ")");					
				}else if(methods.containsKey(content)){
					String parameter = methods.get(content);
					if(parameter.toLowerCase().contains("void")) {
						actualCode = line.replace("Print:", "print").replace("[","(").replace("]", "())");
					}else {
						actualCode = line.replace("Print:", "print").replace("[","(").replace("]", ")");
					}
					
				}else {
					actualCode = line.replace("Print:", "print").replace("[","(\"").replace("]", "\")");					
				}
				sourceCode+=actualCode+"\n";				
			}else if(line.contains("Variable")) {
				int tab_index = line.indexOf("V");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Variable")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String variableName= items[variable_index+2].replace("[", "").replace("]", "");
				String variableValue = items[variable_index+3].replace("[", "").replace("]", "");
				variables.add(variableName);
				actualCode = tab+variableName+" = "+variableValue;
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Equal")) {
				int tab_index = line.indexOf("E");
				int variable_index = line.indexOf(":");
				String tab = line.substring(0,tab_index);
				String next = line.substring(variable_index+2);
				actualCode = next.replace("[","").replace("] = ["," = ").replace("]","");
				actualCode = tab+actualCode;
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Array")) {
				int tab_index = line.indexOf("A");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Array")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String name = items[variable_index+2].replace("[", "").replace("]", "");
				arrayLists.add(name);
				actualCode = tab+name+" = []";
				sourceCode+=actualCode+"\n";				
			}else if(line.contains("Append")) {
				int tab_index = line.indexOf("A");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Append")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String arrayName = items[variable_index+1].replace("[", "").replace("]", "");
				String value = items[variable_index+2].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".append("+value+")";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Insert")) {
				int tab_index = line.indexOf("I");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Insert")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String arrayName = items[variable_index+1].replace("[", "").replace("]", "");
				String location = items[variable_index+2].replace("[", "").replace("]", "");
				String value = items[variable_index+3].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".insert("+location+", "+value+")";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Delete")) {
				int tab_index = line.indexOf("D");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Delete")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String arrayName = items[variable_index+1].replace("[", "").replace("]", "");
				String location = items[variable_index+2].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".pop("+location+")";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Func")) {
				String[] itemFragments = line.split("]");
				String method = "def";
				String methodName = itemFragments[1].replace("[", "");
				String parameter = itemFragments[2].replace("[","(")+"):";
				sourceCode+=method+methodName+parameter+"\n";
			}else if(line.contains("Method")) {
				String[] itemFragments = line.split("]");
				int tab_index = line.indexOf("M");
				String tab = "";
				if(tab_index>0) {
					tab = line.substring(0,tab_index);					
				}
				String returnType = itemFragments[0].replace("method [", "");
				String methodName = itemFragments[1].replace(" [", "");
				String parameter = itemFragments[2].replace("[","(")+")";
				methods.put(methodName.replace(" ", ""),returnType);
				if(returnType.toLowerCase().contains("void")) {
					sourceCode+=tab+methodName+parameter+"\n";					
				}else {
					sourceCode+=tab+methodName+" = "+methodName+parameter+"\n";					
				}
			}else if(line.contains("Start") || line.contains("}")) {
				sourceCode+="\n";
			}else if(line.contains("Return")) {
				String returnLine= line.toLowerCase().replace("[", "").replace("]", "");
				sourceCode += returnLine+"\n";
			}
		}
		return sourceCode;
	}
	
	// C language conversion part
	public String c_language(String pseudocode) {
		String sourceCode = "";
		String[] pseudocodeLines = pseudocode.split("\\n");
		String libraryPart = "#include <stdio.h>\n#include <stdlib.h>\n";
		HashMap<String,Integer> arraySize = new HashMap<>();
		// Save the list of array, function and variable that uses in print part. Because, variables or arrays do not use ' " ' when it prints on the console.
		HashMap<String,String> arrayType = new HashMap<>();
		HashMap<String,String> variables = new HashMap<>();
		HashMap<String,String> methodList = new HashMap<>();
		// for each line in total code
		for(String line : pseudocodeLines) {
			String actualCode = "";
			if(line.contains("If")) {
				actualCode = line.replace("If", "if").replace("[", "(").replace("]", ")");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("While")) {
				actualCode = line.replace("While", "while").replace("[", "(").replace("]", ")");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("}else{")) {
				sourceCode+=line+"\n";
			}else if(line.contains("For")) {
				int tab_index = line.indexOf("F");
				String initialValue="";
				String specificValue = "";
				String stepVal = "";
				String[] items = line.split("]");
				for(String test : items) {
					if(test.contains("For")) {
						int initial_value_index = test.indexOf("[");
						initialValue = test.substring(initial_value_index+1);
					}else if(test.contains("to")) {
						int specific_value_index = test.indexOf("[");
						specificValue = test.substring(specific_value_index+1);
					}else if(test.contains("step")) {
						int step_value_index = test.indexOf("[");
						stepVal = test.substring(step_value_index+1);
					}
				}
				actualCode = line.substring(0,tab_index)+"for(int loop_range="+initialValue+";"+"loop_range<"+specificValue+";"+"loop_range=loop_range+"+stepVal+"){";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Print")) {
				actualCode = line.replace("Print:", "printf").replace(" [","(").replace("]", ");");
				sourceCode+=actualCode+"\n";				
			}else if(line.contains("Variable")) {
				int tab_index = line.indexOf("V");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Variable")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String variableType = items[variable_index+1].replace("[", "").replace("]", "");
				String variableName= items[variable_index+2].replace("[", "").replace("]", "");
				String variableValue = items[variable_index+3].replace("[", "").replace("]", "");
				variables.put(variableName, variableType);
				actualCode = tab+variableType+" "+variableName+" = "+variableValue+";";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Equal")) {
				int tab_index = line.indexOf("E");
				int variable_index = line.indexOf(":");
				String tab = line.substring(0,tab_index);
				String content = line.substring(variable_index+2);
				actualCode = content.replace("[","").replace("] = ["," = ").replace("]","");
				actualCode = tab+actualCode+";";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Array")) {
				int array_index = 0;
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Array")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String type = items[array_index+1].replace("[", "").replace("]", "");
				String name = items[array_index+2].replace("[", "").replace("]", "");
				arrayType.put(name,type);
				arraySize.put(name, 0);
				sourceCode+=line+"\n";				
			}else if(line.contains("Append")) {
				int array_index = 0;
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Append")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String arrayName = items[array_index+1].replace("[", "").replace("]", "");
				String value = items[array_index+2].replace("[", "").replace("]", "");
				if(arrayType.get(arrayName).contains("int")) {
					arraySize.put(arrayName,arraySize.get(arrayName)+1);					
				}else if(arrayType.get(arrayName).contains("char")) {
					arraySize.put(arrayName,arraySize.get(arrayName)+value.length());
				}
				sourceCode+=line+"\n";
			}else if(line.contains("Insert")) {
				int array_index = 0;
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Insert")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String arrayName = items[array_index+1].replace("[", "").replace("]", "");
				String location = items[array_index+2].replace("[", "").replace("]", "");
				String value = items[array_index+3].replace("[", "").replace("]", "");
				if(arrayType.get(arrayName).contains("int")) {
					if(arraySize.get(arrayName)<Integer.parseInt(value)) {
						arraySize.put(arrayName,arraySize.get(arrayName)+Integer.parseInt(location));											
					}else {
						arraySize.put(arrayName,arraySize.get(arrayName)+1);					
					}
				}else if(arrayType.get(arrayName).contains("char")) {
					arraySize.put(arrayName,arraySize.get(arrayName)+value.length());
				}
				sourceCode+=line+"\n";
			}else if(line.contains("Remove")) {
				sourceCode+=line+"\n";
			}else if(line.contains("Func")) {
				String[] itemFragments = line.split("]");
				String returnType = itemFragments[0].replace("Func [", "");
				String methodName = itemFragments[1].replace("[", "");
				String parameter = itemFragments[2].replace(" [","(")+")";
				methodList.put(methodName.replace(" ", ""), returnType+";"+parameter.replace(" ", ""));
				sourceCode+="Func"+returnType+methodName+parameter+"{\n";
			}else if(line.contains("Method")) {
				String[] itemFragments = line.split("]");
				int tab_index = line.indexOf("M");
				String tab = "";
				if(tab_index>0) {
					tab = line.substring(0,tab_index);					
				}
				String returnType = itemFragments[0].replace("Method [", "");
				String methodName = itemFragments[1].replace(" [", "");
				String parameter = itemFragments[2].replace(" [","(")+")";				
				if(returnType.toLowerCase().contains("void")) {
					sourceCode+=tab+methodName+parameter+";\n";
				}else {
					sourceCode+=returnType+" return_"+methodName+" = "+methodName+parameter+";\n";
				}
			}else if(line.contains("Start")||line.contains("End")) {
				sourceCode+="\n"+line+"\n";
			}else if(line.contains("}")) {
				sourceCode+=line+"\n";
			}else if(line.contains("Return")) {
				String returnLine= line.toLowerCase().replace("[", "").replace("]", ";");
				sourceCode += returnLine+"\n";
			}
		}
		if(!arrayType.isEmpty()) {
			libraryPart+="#include <string.h>\n\n\n";
		}
		sourceCode = combineCsourceCode(sourceCode, arrayType, arraySize, variables, methodList);
		sourceCode = libraryPart+sourceCode;
		return sourceCode;
	}
	
	// JAVA language conversion part
	public String java_language(String pseudocode) {
		String sourceCode = "";
		String[] pseudocodeLines = pseudocode.split("\\n");
		String libraryPart = "";
		// Save the list of array, method and variable that uses in print part. Because, variables or arrays do not use ' " ' when it prints on the console.
		ArrayList<String> arrayLists = new ArrayList<>();
		HashMap<String,String> variables = new HashMap<>();
		HashMap<String,String> methods = new HashMap<>();
		// for each line in total code
		for(String line : pseudocodeLines) {
			String actualCode = "";
			if(line.contains("If")) {
				actualCode = line.replace("If", "if").replace("[", "(").replace("]", ")");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("While")) {
				actualCode = line.replace("While", "while").replace("[", "(").replace("]", ")");
				sourceCode+=actualCode+"\n";
			}else if(line.contains("}else{")) {
				sourceCode+=line+"\n";
			}else if(line.contains("For")) {
				int tab_index = line.indexOf("F");
				String initialValue="";
				String specificValue = "";
				String stepVal = "";
				String[] items = line.split("]");
				for(String test : items) {
					if(test.contains("For")) {
						int initial_value_index = test.indexOf("[");
						initialValue = test.substring(initial_value_index+1);
					}else if(test.contains("to")) {
						int specific_value_index = test.indexOf("[");
						specificValue = test.substring(specific_value_index+1);
					}else if(test.contains("step")) {
						int step_value_index = test.indexOf("[");
						stepVal = test.substring(step_value_index+1);
					}
				}
				actualCode = line.substring(0,tab_index)+"for(int i="+initialValue+";"+"i<"+specificValue+";"+"i=i+"+stepVal+"){";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Print")) {
				int tabIndex = line.indexOf("P");
				String content = line.substring(tabIndex).replace("Print: ", "").replace("[","").replace("]", "");
				if(variables.containsKey(content) || arrayLists.contains(content)) {
					actualCode = line.replace("Print:", "System.out.println").replace(" [","(").replace("]", ");");					
				}else if(methods.containsKey(content)){
					String returnType = methods.get(content);
					if(returnType.toLowerCase().contains("void")) {
						
					}else {
						actualCode = line.replace("Print:", "System.out.println").replace(" [","(").replace("]", ");");
					}
				}else {
					actualCode = line.replace("Print:", "System.out.println").replace(" [","(\"").replace("]", "\");");
				}
				sourceCode+=actualCode+"\n";				
			}else if(line.contains("Variable")) {
				int tab_index = line.indexOf("V");
				int variable_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Variable")) {
						break;
					}else {
						variable_index+=1;
					}
				}
				String variableType = items[variable_index+1].replace("[", "").replace("]", "");
				String variableName= items[variable_index+2].replace("[", "").replace("]", "");
				String variableValue = items[variable_index+3].replace("[", "").replace("]", "");
				variables.put(variableName,variableType);
				actualCode = tab+variableType+" "+variableName+" = "+variableValue+";";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Equal")) {
				int tab_index = line.indexOf("E");
				int syntax_index = line.indexOf(":");
				String tab = line.substring(0,tab_index);
				String content = line.substring(syntax_index+2);
				actualCode = content.replace("[","").replace("] = ["," = ").replace("]","");
				actualCode = tab+actualCode+";";
				sourceCode+=actualCode+"\n";
			}else if(line.contains("Array")) {
				int tab_index = line.indexOf("A");
				int array_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Array")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String type = items[array_index+1].replace("[", "").replace("]", "");
				String name = items[array_index+2].replace("[", "").replace("]", "");
				switch(type.toLowerCase()) {
					case "int":
						type = "Integer";
						break;
					case "double":
						type = "Double";
						break;
					case "char":
						type = "Character";
						break;
					case "string":
						type = "String";
						break;
					default:
						break;
				}
				arrayLists.add(name);
				actualCode = tab+"ArrayList<"+type+"> "+name+" = new ArrayList<>();";
				sourceCode+=actualCode+"\n";				
			}else if(line.contains("Append")) {
				int tab_index = line.indexOf("A");
				int array_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Append")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String arrayName = items[array_index+1].replace("[", "").replace("]", "");
				String value = items[array_index+2].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".add("+value+")";
				sourceCode+=actualCode+";\n";
			}else if(line.contains("Insert")) {
				int tab_index = line.indexOf("I");
				int array_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Insert")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String arrayName = items[array_index+1].replace("[", "").replace("]", "");
				String location = items[array_index+2].replace("[", "").replace("]", "");
				String value = items[array_index+3].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".add("+location+", "+value+")";
				sourceCode+=actualCode+";\n";
			}else if(line.contains("Remove")) {
				int tab_index = line.indexOf("R");
				int array_index = 0;
				String tab = line.substring(0,tab_index);
				String[] items = line.split("\\s");
				for(String item : items) {
					if(item.contains("Remove")) {
						break;
					}else {
						array_index+=1;
					}
				}
				String arrayName = items[array_index+1].replace("[", "").replace("]", "");
				String location = items[array_index+2].replace("[", "").replace("]", "");
				actualCode = tab+arrayName+".remove("+location+")";
				sourceCode+=actualCode+";\n";
			}else if(line.contains("Func")) {
				String[] itemFragments = line.split("]");
				String returnType = itemFragments[0].replace("Func [", "public static ");
				String methodName = itemFragments[1].replace("[", "");
				String parameter = itemFragments[2].replace("[","(")+"){";
				sourceCode+="Func"+returnType+methodName+parameter+"\n";
			}else if(line.contains("Method")) {
				String[] itemFragments = line.split("]");
				int tab_index = line.indexOf("M");
				String tab = "";
				if(tab_index>0) {
					tab = line.substring(0,tab_index);					
				}
				String returnType = itemFragments[0].replace("Method [", "");
				String methodName = itemFragments[1].replace(" [", "");
				String parameter = itemFragments[2].replace("[","(")+")";
				methods.put(methodName.replace(" ", ""), returnType);
				if(returnType.toLowerCase().contains("void")) {
					sourceCode+=tab+methodName+parameter+";\n";					
				}else {
					sourceCode+=returnType+" "+methodName+" = "+methodName+parameter+";\n";
				}
			}else if(line.contains("Start")||line.contains("End")) {
				sourceCode+="\n"+line+"\n";
			}else if(line.contains("}")) {
				sourceCode+=line+"\n";
			}else if(line.contains("Return")) {
				String returnLine= line.toLowerCase().replace("[", "").replace("]", ";");
				sourceCode += returnLine+"\n";
			}
		}
		if(!arrayLists.isEmpty()) {
			libraryPart += "import java.util.ArrayList;\n";
		}
		sourceCode = combineJavaSourceCode(sourceCode,variables);
		sourceCode = libraryPart+"\n"+sourceCode;
		return sourceCode;
	}
	
	public String makeSourceCode(String type, String pseudocode) {
		String languageType = type;
		String sourceCode = "";
		switch(languageType) {
			case "python":
				sourceCode = python_language(pseudocode);
				break;
			case "c":
				sourceCode = c_language(pseudocode);
				break;
			case "java":
				sourceCode = java_language(pseudocode);
				break;
			default:
				break;
		}
		return sourceCode;
	}
}
