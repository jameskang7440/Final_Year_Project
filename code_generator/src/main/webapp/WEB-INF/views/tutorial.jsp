<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>                        
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>
  
	<div class="container-fluid text-center" style="padding-bottom:100px;">    
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
	      		<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial" class="active">Tutorial Home</a></p>
	      		<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/basic">Basic part</a></p>
	      		<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/operation">Operation part</a></p>
	      		<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/array">Array part</a></p>
	      		<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/statement">Statement part</a></p>
	      		<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/function">Function part</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left"> 
	      		<h1>Code Generator Tutorial</h1>
	    		<div class="cg-panel cg-info intro">
	    			<p>With your own flowchart, you can create your own programming source code.</p>
	    			<p>This tutorial helps you about handling flowchart items.</p>
	    			<p>Those tutorials are easy to understand - You will success to build your own.</p>
	    		</div> 
	    		<hr>
	    		<h3>Examples in all chapters</h1>
	    		<p>This tutorial contains lots of flowchart examples within the example images.</p>
	    		<p>You can understand how to use those flowchart items from those.</p>
	    		<p>We hope to be able to successfully generate your code from our flowchart system. Good Luck!!</p>
	    	</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->