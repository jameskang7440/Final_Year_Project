<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>                        
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>
  
	<div class="container-fluid text-center" style="padding-bottom:100px;">    
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial">Tutorial Home</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/basic">Basic part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/operation">Operation part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/array">Array part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/statement">Statement part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial/function" class="active">Function part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/example">Example</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left"> 
	      		<h1>Function part Tutorial</h1>
	    		<div class="cg-panel cg-info intro">
	    			<p>With your own flowchart, you can create your own programming source code.</p>
	    			<p>This tutorial helps you about handling flowchart items.</p>
	    			<p>Those tutorials are easy to understand - You will success to build your own.</p>
	    		</div> 
	    		<h3><a name="contents"></a>Contents</h3>
				<dl name="contents" class="toc-indent">
					<dt><span><a href="#s-1">1</a>. Method</span></dt>
					<dt><span><a href="#s-2">2</a>. Print function</span></dt>
				</dl>
				<br>
				<span><h2><a id="s-1" href="#contents">1.</a> Method</h2></span>
				<br>
				<p>A method is a group of statements that together perform a task.</p>
				<p>Now you will learn how to create your own methods with or without return values, invoke a method with or without parameters.</p>
				<h2>Generate a method </h2>
				<p>Syntax</p>
				<img src="/resources/images/function/method/method_raw.png"/>
				<p>return type − A function may return a value. The return_type is the data type of the value the function returns. Some functions perform the desired operations without returning a value. In this case, the return_type is the keyword void.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>If you need to return the value (adapt return type in method), you must use the return item which seems like this :</p>
						<img src="/resources/images/function/method/method_return.png"/>
						<p>You can put the variable name or the specific value inside of the bracket [ ].</p>
					</div>
				</div>
				<p>name − This is the actual name of the function. The function name and the parameter list together constitute the function signature.</p>				
				<p>parameter − A parameter is like a placeholder. When a function is invoked, you pass a variable to the parameter. Parameters are optional; that is, a function may contain no parameters.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>Parameters in our flowchart only accept the variable(s) which was already declared before.</p>
						<p>So, if you want to use parameters, generate variable(s) first then put those into inside of the parameter section([parameter]).</p>
						<p>Example of using parameter in the method:</p>
						<img src="/resources/images/function/method/method_using_parameter.png"/>
					</div>
				</div>
				<br>
				<h2>Examples of using method</h2>
				<br>
				<p><b>void type:</b></p>
				<img src="/resources/images/function/method/method_void.png"/>
				<p>This example prints the 'text' variable (String type) inside of the method.</p>
				<br>
				<p><b>Character type:</b></p>
				<img src="/resources/images/function/method/method_char.png"/>
				<p>This example exchanges the uppercase character from the lowercase character (from Character type variable) inside of the method and then return the result.</p>
				<br>
				<p><b>Integer type:</b></p>
				<img src="/resources/images/function/method/method_int.png"/>
				<p>This example implements the addition between two integer values. This method returns the result of addition.</p>
				<br>
				<p><b>Double type:</b></p>
				<img src="/resources/images/function/method/method_double.png"/>
				<p>This example calculates the fahrenheit degree from the celsius degree.</p>
				<br>
				<p><b>String type:</b></p>
				<img src="/resources/images/function/method/method_string.png"/>
				<p>This example generates the greeting message. This method returns 'Hello James'.</p>
				<br>
				<hr>
				<span><h2><a id="s-2" href="#contents">2.</a> Print function</h2></span>
				<br>
				<p>The print function in this flowchart can print the value, variable, array and function.</p>
				<p>Thus, if you want to see something on the console, use the print function</p>
				<p>Syntax</p>
				<img src="/resources/images/function/print/print_raw.png"/>
				<p>[plaintext or variable] - you can put the plain text, number value, variable, array or function inside when you want to see those</p>
				<div class="panel panel-info">
					<div class="panel-heading">INFORMATION</div>
					<div class="panel-body">
						<p>If you want to print only plain text, you do not need to use quotation marks.</p>
						<p>Example of printing plain text(s) in the print function:</p>
						<img src="/resources/images/function/print/print_plain.png"/>
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>However, it can not print the plain text and variable(s) or method together. Thus, you should separate the plain text part and variable(s) or method part.</p>
						<p>Example of printing plain text(s) and variables or method in the print function (JAVA Style):</p>
						<p><img src="/resources/images/function/print/print_mix1.png"/> = <img src="/resources/images/function/print/print_mix2.png"/></p>
					</div>
				</div>
				<h2>Examples of using print function</h2>
				<br>
				<p>Examples:</p>
				<p><b>Print variable:</b></p>
				<img src="/resources/images/function/print/print_variable.png"/>
				<br>
				<p><b>Print array:</b></p>
				<img src="/resources/images/function/print/print_array.png"/>
				<br>
				<p><b>Print function:</b></p>
				<img src="/resources/images/function/method/method_string.png"/>
				<br>
				<br>
			</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->