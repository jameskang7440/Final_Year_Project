<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<!--  -->
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Flowchart</title>
<meta name="description" content="Interactive flowchart diagram implemented by GoJS in JavaScript for HTML." />
<!-- Copyright 1998-2018 by Northwoods Software Corporation. -->
<meta charset="UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="../resources/css/style.css" />
<script src="../resources/js/go.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<body>
<div id="body">
	<nav class="navbar navbar-inverse navbar-custom">
  		<div class="container-fluid">
    		<div class="navbar-header">
      			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
        			<span class="icon-bar"></span>
      			</button>
      			<a class="navbar-brand" href="/">Code Generator</a>
    		</div>
    		<div class="collapse navbar-collapse" id="myNavbar">
      			<ul class="nav navbar-nav">
        			<li><a href="/">Home</a></li>
        			<li><a href="#">About</a></li>
        			<li><a href="#">Contact</a></li>
      			</ul>
    		</div>
  		</div>
	</nav>

	<div class="container-fluid text-center" style="padding-bottom:100px;">
	  	<div class="row content">
	    	<div class="col-sm-2 sidenav">
	      		<h4>Code Generator Tutorial</h4>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial">Tutorial Home</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/basic">Basic part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/operation">Operation part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/array">Array part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px; background-color: #4CAF50;"><a href="/tutorial/statement" class="active">Statement part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/function">Function part</a></p>
      			<p style="line-height: 50px; margin-bottom: 0px;"><a href="/tutorial/example">Example</a></p>
	    	</div>
	    	<div class="col-sm-8 text-left">
	      		<h1>Statement part Tutorial</h1>
	    		<div class="cg-panel cg-info intro">
	    			<p>With your own flowchart, you can create your own programming source code.</p>
	    			<p>This tutorial helps you about handling flowchart items.</p>
	    			<p>Those tutorials are easy to understand - You will success to build your own.</p>
	    		</div>
				<h3><a name="contents"></a>Contents</h3>
				<dl name="contents" class="toc-indent">
					<dt><span><a href="#s-1">1</a>. If statement</span></dt>
					<dt><span><a href="#s-2">2</a>. For statement</span></dt>
					<dt><span><a href="#s-3">3</a>. While statement</span></dt>
				</dl>
				<br>
				<span><h2><a id="s-1" href="#contents">1.</a> If statement</h2></span>
				<br>
				<p>The if statement is one of the conditional statements. It performs different computations or actions depending on whether a programmer-specified condition evaluates to true or false.</p>
				<h2>How to use if statement in the flowchart</h2>
				<p>In this shape, you can edit the condition statement inside of the [ ] bracket. And also, you must plot the 'End if' item when you finish to make if statement. To create the contents of an if statement, you must link the node from the 'True' port of the if statement. Similarly, to create the contents of an else statement, you must link the node from the 'False' port of the if statement. Then, if you have created the contents of the if and else statements, you must concatenate the contents of the last item to the 'End if' port. Then connect the next item through the bottom port of 'End if'.</p>
				<p>Here is the initial item of the if statement in the flowchart board :</p>
				<img src="/resources/images/statement/if/statement_if_raw.png"/>
				<br>
				<h2>Examples of using if statement</h2>
				<p>Ex 1) Determine if the variable value is greater than or equal to 10:</p>
				<img src="/resources/images/statement/if/statement_if_ex1.png"/>
				<br><br>
				<p>Ex 2) Determine if the letter of a character is 'a' or not :</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>You must use single quotation marks when you use character letter in conditional statement.</p>
					</div>
				</div>
				<img src="/resources/images/statement/if/statement_if_ex2.png"/>
				<br><br>
				<p>Ex 3) Determine if the person's name is 'Alex' or not :</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>You must use double quotation marks when you use string type value in conditional statement.</p>
					</div>
				</div>
				<img src="/resources/images/statement/if/statement_if_ex3.png"/>
				<br><br>
				<p>Also, you can make your own if statement similar to the example.</p>
				<hr>
				<span><h2><a id="s-2" href="#contents">2.</a> For statement</h2></span>
				<br>
				<p>The for statement is a control flow statement for specifying iteration, which allows code to be executed repeatedly.</p>
				<h2>How to use for statement in the flowchart</h2>
				<p>In this shape, you can edit the minimum value, maximum value and step value inside of those [ ] brackets. Those three options should be the Integer value and you must fill it. To create the contents inside the for statement, you must connect the node from the 'True' port of for statement. After that, if you have created the contents of for statement, you have to complete the contents by connecting to the 'continue' port of for statement. You can then link the next item through the 'escape' port of for statement.</p>
				<div class="panel panel-warning">
					<div class="panel-heading">WARNING</div>
					<div class="panel-body">
						<p>The maximum value is not included in the for loop range. So the for loop range is 'minimum value' &le; loop count(s) &lt; 'maximum value'.</p>
						<p>For example, if user insert '0' for minimum value ,'10' for maximum value and '1' for step value, the for loop range is from 0 to 9.</p>
					</div>
				</div>
				<p>Here is the initial item of the for statement in the flowchart board : </p>
				<img src="/resources/images/statement/for/statement_for_raw.png"/>
				<br>
				<h2>Examples of using for statement</h2>
				<p>Ex 1) add 1 to 10 times:</p>
				<img src="/resources/images/statement/for/statement_for_ex1.png"/>
				<br><br>
				<p>Ex 2) add 'a' to 10 times in an array:</p>
				<img src="/resources/images/statement/for/statement_for_ex2.png"/>
				<br><br>
				<p>Also, you can make your own for statement similar to the example.</p>
				<hr>
				<span><h2><a id="s-3" href="#contents">3.</a> While statement</h2></span>
				<br>
				<p>The while statement is a control flow statement that allows code to be executed repeatedly based on a given condition. It can be thought of as a repeating if statement.</p>
				<h2>How to use while statement in the flowchart</h2>
				<p>In this shape, you can edit the condition statement inside of the [ ] bracket. To create the contents inside the while statement, you must connect the node from the 'True' port of for statement. After that, if you have created the contents of while statement, you have to complete the contents by connecting to the 'continue' port of while statement. You can then link the next item through the 'escape' port of while statement.</p>
				<p>Here is the initial item of the while statement in the flowchart board : </p>
				<img src="/resources/images/statement/while/statement_while_raw.png"/>
				<br>
				<h2>Examples of using while statement</h2>
				<p>Ex 1) make positive integer from the negative integer</p>
				<img src="/resources/images/statement/while/statement_while_ex1.png"/>
				<br><br>
				<p>Also, you can make your own while statement similar to the example.</p>
				<hr>
			</div>
	    	<div class="col-sm-2 sidenav">
	      		<div class="well">
	        		<p>ADS</p>
	      		</div>
	      		<div class="well">
	       	 		<p>ADS</p>
	      		</div>
	    	</div>
	    </div>
	</div>
</div>

<div id="footer" class="text-center">
	<p>Designed by Janghoon Kang</p>
</div>
</body>
</html>
<!--  -->
